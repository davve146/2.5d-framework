extends Position3D

onready var tween = $Tween
var move_dir = Vector3()
var last_pos = Vector3()
var target_pos = Vector3()
var direction = 0
var level = 2
var inputDisabled = false

# 0 = passable, 1 = impassable, 2 = Stairs, 3 = surfable
onready var collision = $Overworld/Collision


var width = 30
var height = 30

export(Resource) var collisionMap

var foot = 0

enum DIRECTION {
	DOWN,
	LEFT,
	RIGHT,
	UP
}

func _ready():
	last_pos = translation
	target_pos = translation

func _process(delta):
	if !inputDisabled:
		get_input()

func get_input():
	if Input.is_action_pressed("ui_down"):
		direction = DIRECTION.DOWN
	elif Input.is_action_pressed("ui_up"):
		direction = DIRECTION.UP
	elif Input.is_action_pressed("ui_left"):
		direction = DIRECTION.LEFT
	elif Input.is_action_pressed("ui_right"):
		direction = DIRECTION.RIGHT
	else:
		set_idle_frame()
		return
	inputDisabled = true
	
	move()


func move():
	set_process(false)
	
	move_dir = Vector3.ZERO
	
	match direction:
		0:
			move_dir.z = 1
		1:
			move_dir.x = -1
		2:
			move_dir.x = 1
		3:
			move_dir.z = -1
	if level == 1.5:
		if direction == DIRECTION.UP:
			#translation.y = 1.7
			move_dir.y = 0.5
			level = 2
		elif direction == DIRECTION.DOWN:
			#translation.y = 0.7
			move_dir.y = -0.5
			level = 1
		
	
	if !collide():
		animate()
		
		tween.interpolate_property(self, "translation", last_pos, last_pos + move_dir, $AnimationPlayer.current_animation_length, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()
		
		yield($AnimationPlayer, "animation_finished")
		
		if foot == 0:
			foot = 1
		else:
			foot = 0
	else:
		last_pos = translation
		inputDisabled = false
		set_process(true)
		set_idle_frame()
		return
	
	last_pos = translation
	
	#print(last_pos)
	inputDisabled = false
	set_process(true)

func set_idle_frame():
	$Sprite3D.frame = direction * 4

func animate():
	#TODO All other animations
	
	
	if foot == 0:
		match direction:
			0:
				$AnimationPlayer.play("down")
			1:
				$AnimationPlayer.play("left")
			2:
				$AnimationPlayer.play("right")
			3:
				$AnimationPlayer.play("up")
	elif foot == 1:
		match direction:
			0:
				$AnimationPlayer.play("down2")
			1:
				$AnimationPlayer.play("left2")
			2:
				$AnimationPlayer.play("right2")
			3:
				$AnimationPlayer.play("up2")

func collide():
	return false
#	#var index = 
#	var x = translation.x + move_dir.x + get_parent().translation.x - 0.5
#	var z = translation.z + move_dir.z + get_parent().translation.z - 0.25
#	var index = width*z + x
#	if index >= width * height or x > width - 1 or x < 0 or z < 0:
#		return true
#	if collision[index] == 0:
#		return false
#	elif collision[index] == 2:
#		if direction == DIRECTION.UP:
#			#translation.y = 1.7
#			move_dir.y = 0.5
#			level = 1.5
#		elif direction == DIRECTION.DOWN:
#			#translation.y = 0.7
#			move_dir.y = -0.5
#			level = 1.5
#		return false
#	else:
#		return true
#	pass
