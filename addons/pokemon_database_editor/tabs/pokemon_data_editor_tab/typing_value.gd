tool
extends OptionButton

var popup

func _ready():	
	popup = get_popup()
	popup.clear()
	
	for type in Types.TYPES.values():
		popup.add_icon_check_item(Types.icons[type], "", type)
