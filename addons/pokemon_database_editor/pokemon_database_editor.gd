tool
extends EditorPlugin

var editor
var open = false
var dir = Directory.new()

#func _enter_tree():
#
#	editor = preload("res://addons/pokemon_database_editor/pokemon_database_editor.tscn").instance()
#	add_control_to_dock(DOCK_SLOT_LEFT_UL, editor)
#
func _exit_tree():

	remove_control_from_docks(editor)	
	editor.free()

func _unhandled_key_input(event):	
	if event.scancode == KEY_F11 and event.pressed == false:
		if !open:
			open = true
			editor = preload("res://addons/pokemon_database_editor/pokemon_database_editor.tscn").instance()
			
			add_control_to_dock(DOCK_SLOT_LEFT_UL, editor)
			#editor.get_node("TabContainer/Pokemon Data/Main/SideBar/Create").connect("pressed", self, "_on_create_new_pokemon", [editor])

			
		
		elif open:
			remove_control_from_docks(editor)	
			#save_all()
			editor.free()
			open = false

func _setup():
	pass
	
func _check_dir() -> bool:
	
	
	
	return false
