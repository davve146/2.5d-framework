tool
extends ColorRect

onready var util = preload("res://utilities/general_utilities.gd")
onready var pokemon_directory = Directory.new()

# Contains both the sidebar instance and data scene
var pokemon_data
var pokemon_list = {}
var editor = EditorPlugin.new()



func _ready():
	_load_all()
	$SideBar/Create.connect("pressed", self, "_on_create_new_pokemon", [self])

func _load_all() -> void:
	
	#loads all pokemon.tres files from /pokemon folder into pokemon_list
	
	
	var pokedex = $SideBar/ColorRect/ScrollContainer/MarginContainer/VBoxContainer
	util.delete_all_children(pokedex)
	
	var stored_pokemon = []
	if pokemon_directory.open("res://Backend/Pokemon") == OK:
		
		
		pokemon_directory.list_dir_begin()
		var file_name = pokemon_directory.get_next()
		
		while file_name != "":

			if not pokemon_directory.current_is_dir():
				var pokemon = load(pokemon_directory.get_current_dir() + "/" + file_name)
				stored_pokemon.append(pokemon)
				print_debug("found pokemon " + pokemon.name + "!")
				
			file_name = pokemon_directory.get_next()
				
	util._sort_by_index(stored_pokemon)
	
	
	
	for pokemon in stored_pokemon:
		
		#creates data packed scene
		
		#General
		
		pokemon_data = load("res://addons/pokemon_database_editor/tabs/pokemon_data_editor_tab/pokemon_data.tscn").instance()
		pokemon_data.get_node("General/IndexNumber/Value").text = str(pokemon.id)
		pokemon_data.get_node("General/SpeciesName/Value").text = pokemon.name
		pokemon_data.get_node("General/InternalName/Value").text = pokemon.internal_name
		pokemon_data.get_node("General/Icon/Value").texture = pokemon.icon
		pokemon_data.get_node("General/GenderRatio/Value").value = pokemon.gender_ratio
		pokemon_data.get_node("General/GrowthRate/Value").select(pokemon.growth_rate)
		pokemon_data.get_node("General/BaseHappiness/Value").value = pokemon.base_happiness
		pokemon_data.get_node("General/CatchRate/Value").value = pokemon.catch_rate
		pokemon_data.get_node("General/EVYield/Value").text = str(pokemon.ev_yield_value)
		pokemon_data.get_node("General/EVYield/ValueType").select(pokemon.ev_yield_type)
		
		# Egg data
		
		pokemon_data.get_node("EggData/EggGroup1/Value").select(pokemon.egg_group_1)
		pokemon_data.get_node("EggData/EggGroup2/Value").select(pokemon.egg_group_2)
		pokemon_data.get_node("EggData/StepsToHatch/Value").text = str(pokemon.steps_to_hatch)
		
		# Type

		pokemon_data.get_node("Type/PrimaryType/Value").select(int(pokemon.type_primary))
		#pokemon_data.get_node("Type/PrimaryType/Value").get_popup().set_item_checked(pokemon.type_primary.id, true)
		pokemon_data.get_node("Type/SecondaryType/Value").select(int(pokemon.type_secondary))
		#pokemon_data.get_node("Type/SecondaryType/Value").get_popup().set_item_checked(pokemon.type_secondary.id, true)
		
		# Pokedex
		
		pokemon_data.get_node("Pokedex/Category/Value").text = pokemon.category
		pokemon_data.get_node("Pokedex/Height/ValueFeet").text = str(pokemon.height_feet)
		pokemon_data.get_node("Pokedex/Height/ValueInches").text = str(pokemon.height_inches)
		pokemon_data.get_node("Pokedex/Weight/Value").text = str(pokemon.weight)
		pokemon_data.get_node("Pokedex/Description/Value").text = pokemon.description
		
		# Base stats
		
		pokemon_data.get_node("Stats/Hp/Value").value = pokemon.base_hp
		pokemon_data.get_node("Stats/Attack/Value").value = pokemon.base_attack
		pokemon_data.get_node("Stats/Defence/Value").value = pokemon.base_defence
		pokemon_data.get_node("Stats/SpecialAttack/Value").value = pokemon.base_special_attack
		pokemon_data.get_node("Stats/SpecialDefence/Value").value = pokemon.base_special_defence
		pokemon_data.get_node("Stats/Speed/Value").value = pokemon.base_speed
		
		# Abilities
		
		
		
		# Moves
		
		
		
		# Sprites
		
		pokemon_data.get_node("Sprites/SpriteSheet/Value").texture = pokemon.sprites
		
		# creates sidebar entry
		
		var pokemon_entry = load("res://addons/pokemon_database_editor/tabs/pokemon_data_editor_tab/pokemon_entry.tscn").instance()
		pokemon_entry.get_node("IndexNumber").text = "#" + str(pokemon.id)
		pokemon_entry.get_node("Name").text = pokemon.name
		pokemon_entry.get_node("Icon").texture = pokemon.icon
		
		pokemon_entry.connect("pressed", self, "_load_data", [pokemon])
		pokemon_entry.get_node("Delete").connect("pressed", self, "_delete_pokemon", [pokemon])
		print_debug("Adding " + pokemon.name + " to dict")
		pokemon_list[pokemon] = [pokemon_data, pokemon_entry]
		$SideBar/ColorRect/ScrollContainer/MarginContainer/VBoxContainer.add_child(pokemon_entry)
		


func _load_data(pokemon: Resource) -> void:
	var pokemon_data_container = $Data/ColorRect/ScrollContainer/MarginContainer/
	if pokemon_data_container.get_child_count() > 0:
		pokemon_data_container.remove_child(pokemon_data_container.get_child(0))
	pokemon_data_container.add_child(pokemon_list[pokemon][0])



func _on_create_new_pokemon(interface):
	
	var pokemon: Resource = load("res://Backend/Pokemon/Scripts/pokemon_data_generic.gd").new()
	pokemon.set_script(load("res://Backend/Pokemon/Scripts/pokemon_data_generic.gd").duplicate(true))
	
	var pokemon_entry = preload("res://addons/pokemon_database_editor/tabs/pokemon_data_editor_tab/pokemon_entry.tscn").instance()
	pokemon_entry.get_node("IndexNumber").text = "#" + str(len(pokemon_list)+1)
	pokemon_entry.get_node("Name").text = pokemon.resource_name
	pokemon_entry.connect("pressed", self, "_load_data", [pokemon])
	pokemon_entry.get_node("Delete").connect("pressed", self, "_delete_pokemon", [pokemon])
	$SideBar/ColorRect/ScrollContainer/MarginContainer/VBoxContainer.add_child(pokemon_entry)
	var pokemon_data = load("res://addons/pokemon_database_editor/tabs/pokemon_data_editor_tab/pokemon_data.tscn").instance()
	pokemon_list[pokemon] = [pokemon_data, pokemon_entry]

func _delete_pokemon(pokemon: Resource) -> void:
	
	var pokemon_data_container = $Data/ColorRect/ScrollContainer/MarginContainer
	if pokemon_data_container.get_child_count() > 0:
		pokemon_data_container.remove_child(pokemon_data_container.get_child(0))
	$SideBar/ColorRect/ScrollContainer/MarginContainer/VBoxContainer.remove_child(pokemon_list[pokemon][1])
	pokemon_list.erase(pokemon)
	var dir = Directory.new()
	dir.remove(pokemon.resource_path)
	
	editor.get_editor_interface().get_resource_filesystem().scan()
	


func _on_SaveAll_pressed():
	
	var error = OK
	var error_pokemon = []
	
	util._remove_all_files("res://Backend/Pokemon/")
	
	for pokemon in pokemon_list.keys():
		
		if pokemon_list[pokemon][0].get_node("General/InternalName/Value").text == "":
			print_debug("Pokemon missing internal name, unable to write file name!")
			error = ERR_INVALID_DATA
			error_pokemon.append(pokemon)
			continue
		
		# General
		
		pokemon.id = int(pokemon_list[pokemon][0].get_node("General/IndexNumber/Value").text)
		pokemon.name = pokemon_list[pokemon][0].get_node("General/SpeciesName/Value").text
		pokemon.internal_name = pokemon_list[pokemon][0].get_node("General/InternalName/Value").text
		pokemon.icon = pokemon_list[pokemon][0].get_node("General/Icon/Value").texture 
		pokemon.gender_ratio = pokemon_list[pokemon][0].get_node("General/GenderRatio/Value").value 
		pokemon.growth_rate = pokemon_list[pokemon][0].get_node("General/GrowthRate/Value").selected
		pokemon.base_happiness = pokemon_list[pokemon][0].get_node("General/BaseHappiness/Value").value
		pokemon.catch_rate = pokemon_list[pokemon][0].get_node("General/CatchRate/Value").value 
		pokemon.ev_yield_value = int(pokemon_list[pokemon][0].get_node("General/EVYield/Value").text)
		pokemon.ev_yield_type = pokemon_list[pokemon][0].get_node("General/EVYield/ValueType").selected
		
		# Egg data
		
		pokemon.egg_group_1 = pokemon_list[pokemon][0].get_node("EggData/EggGroup1/Value").selected
		pokemon.egg_group_2 = pokemon_list[pokemon][0].get_node("EggData/EggGroup2/Value").selected
		pokemon.steps_to_hatch = int(pokemon_list[pokemon][0].get_node("EggData/StepsToHatch/Value").text)
		
		# Type
		
		#pokemon.type_primary = int(pokemon_list[pokemon][0].get_node("Type/PrimaryType/Value").selected)
		#pokemon.type_secondary = int(pokemon_list[pokemon][0].get_node("Type/SecondaryType/Value").selected)
		
		# Pokedex
		
		pokemon.category = pokemon_list[pokemon][0].get_node("Pokedex/Category/Value").text
		pokemon.height_feet = int(pokemon_list[pokemon][0].get_node("Pokedex/Height/ValueFeet").text)
		pokemon.height_inches = int(pokemon_list[pokemon][0].get_node("Pokedex/Height/ValueInches").text)
		pokemon.weight = int(pokemon_list[pokemon][0].get_node("Pokedex/Weight/Value").text)
		pokemon.description = pokemon_list[pokemon][0].get_node("Pokedex/Description/Value").text
		
		# Base stats
		
		pokemon.base_hp = pokemon_list[pokemon][0].get_node("Stats/Hp/Value").value
		pokemon.base_attack = pokemon_list[pokemon][0].get_node("Stats/Attack/Value").value
		pokemon.base_defence = pokemon_list[pokemon][0].get_node("Stats/Defence/Value").value
		pokemon.base_special_attack = pokemon_list[pokemon][0].get_node("Stats/SpecialAttack/Value").value
		pokemon.base_special_defence = pokemon_list[pokemon][0].get_node("Stats/SpecialDefence/Value").value
		pokemon.base_speed = pokemon_list[pokemon][0].get_node("Stats/Speed/Value").value
		
		# Abilities
		
		
		
		# Moves
		
		
		
		# Sprites
		
		pokemon.sprites = pokemon_list[pokemon][0].get_node("Sprites/SpriteSheet/Value").texture
		
		print_debug("Attempting to save ", pokemon.name, " to ", pokemon_directory.get_current_dir() + "...")

		if ResourceSaver.save(pokemon_directory.get_current_dir() + "/" + pokemon.internal_name + ".tres", pokemon) == OK:

			print_debug("success!")
		
	return [error, error_pokemon]


func _on_Refresh_pressed():
	var results = _on_SaveAll_pressed()
	
	if results[0] != OK:
		print_debug("Couldn't save all pokemon correctly, missing internal names\nDeleting entries...\n")
	
	pokemon_list.clear()
	_load_all()
