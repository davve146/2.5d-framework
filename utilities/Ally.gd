extends "res://utilities/battle/database/pokemon/471.gd"

var level = 50

var HP = ((((base_hp + iv_hp) * 2 + (sqrt(ev_yield_hp) / 4)) * level) / 100) + level + 10
var Attack = ((((base_attack + iv_attack) * 2 + (sqrt(ev_yield_attack) / 4)) * level) / 100) + 5
var Defense = ((((base_defense + iv_defense) * 2 + (sqrt(ev_yield_defense) / 4)) * level) / 100) + 5
var SpecialAttack = ((((base_sp_attack + iv_sp_attack) * 2 + (sqrt(ev_yield_sp_attack) / 4)) * level) / 100) + 5
var SpecialDefense = ((((base_sp_defense + iv_sp_defense) * 2 + (sqrt(ev_yield_sp_defense) / 4)) * level) / 100) + 5
var Speed = ((((base_speed + iv_speed) * 2 + (sqrt(ev_yield_speed) / 4)) * level) / 100) + 5

var currentHP = HP

var moves = [
	{
			"name": "Ice Fang",
			"power": 65,
			"type": Types.TYPES.ICE,
			"category": "physical"
		}
]
