tool
extends HSlider

func _ready():
	$ValueF.text = str(round(value*100))
	$ValueM.text = str(round(100-value*100))

func _on_value_changed(new_value):
	value = new_value
	$ValueF.text = str(round(value*100))
	$ValueM.text = str(round(100-value*100))

func _on_ValueF_text_entered(new_text):
	value = float(new_text)/100
	$ValueM.text = str(abs(round((1-value)*100)))


func _on_ValueM_text_entered(new_text):
	value = 1 - float(new_text)/100
	$ValueF.text = str(abs(round(value*100)))
