tool
extends Container

onready var typings = []
onready var dir = Directory.new()

var Types = preload("res:/Backend/TypeCharts/MainChart.tres")

func _ready():
	
	for type in Types.TYPES.values():
		var icon = TextureRect.new()
		print_debug(int(type))
		icon.texture = Types.icons[int(type)]
		add_child(icon)
