tool
extends ColorRect

onready var util = preload("res://utilities/general_utilities.gd")
onready var typing_directory = Directory.new()

# Contains both the sidebar instance and data scene
var row
var editor = EditorPlugin.new()
var types = preload("res://Backend/TypeCharts/MainChart.tres")

func _ready():
	_load_All()


func _load_All() -> void:
	
	for type in types.TYPES.values():
		
		var row = HBoxContainer.new()
		
		for i in range(len(types.TYPES)):
			var cell = load("res://addons/pokemon_database_editor/tabs/typing_chart_editor_tab/type_chart_cell.tscn").instance()
			row.add_child(cell)
		
		
		print_debug("analysing relations...")
		
		for i in range(len(types.EFFECTIVENESS[int(type)])):
				print(types.EFFECTIVENESS[int(type)][i])
				if types.EFFECTIVENESS[int(type)][i] == 0.5 or types.EFFECTIVENESS[int(type)][i] == 2 or types.EFFECTIVENESS[int(type)][i] == 0:
					row.get_child(i).text = str(types.EFFECTIVENESS[int(type)][i])
					pass
				
		
		$ScrollContainer/ColorRect/VBoxContainer.add_child(row)


func _on_Save_pressed():

	for row in $ScrollContainer/ColorRect/VBoxContainer.get_children():
		var cells = row.get_children()

		for cell in cells:
			#print_debug(cell.text)
			if cell.text == "0" or cell.text == "0.5" or cell.text == "2":
				types.EFFECTIVENESS[int(row.get_index())][int(cell.get_index())] = float(cell.text)
			else:
				types.EFFECTIVENESS[int(row.get_index())][int(cell.get_index())] = 1
		
	
