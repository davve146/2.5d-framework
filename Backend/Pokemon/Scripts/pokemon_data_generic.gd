extends Resource
class_name PokemonDataGeneric

enum GrowthRate { 
	ERRATIC,
	FAST,
	MEDIUM_FAST,
	MEDIUM_SLOW,
	SLOW,
	FLUCTUATING 
}

enum EvYield {
	HP,
	ATTACK,
	DEFENCE,
	SP_ATK,
	SP_DEF,
	SPEED
}

enum egg_group {
	AMORPHOUS,
	BUG,
	DRAGON,
	FAIRY,
	FIELD,
	FLYING,
	GRASS,
	HUMAN_LIKE,
	MINERAL,
	MONSTER,
	WATER_1,
	WATER_2,
	WATER_3,
	DITTO,
	UNDISCOVERED
}

# General

export(String) var name: String
export(int) var id: int
export(String) var internal_name: String
export(float) var gender_ratio: float
export(GrowthRate) var growth_rate
export(int) var base_happiness: int
export(int) var catch_rate: int
export(int) var base_exp: int
export(EvYield) var ev_yield_type
export(int) var ev_yield_value

# Egg Data

export(egg_group) var egg_group_1
export(egg_group) var egg_group_2
export(int) var steps_to_hatch

# Typing

export(Types.TYPES) var type_primary
export(Types.TYPES) var type_secondary

# Pokedex

export(String) var category
export(int) var height_feet
export(int) var height_inches
export(int) var weight
export(String) var description
export(Texture) var footprint

# Base stats

export(int) var base_hp: int
export(int) var base_attack: int
export(int) var base_defence: int
export(int) var base_special_attack: int
export(int) var base_special_defence: int
export(int) var base_speed: int

# Abilities

var ability_1: Resource
var ability_2: Resource

# Moves

var level_up_learnset = {}

# Sprites

export(Texture) var sprites
export(SpriteFrames) var icon
export(SpriteFrames) var battle_sprites_front_male
export(SpriteFrames) var battle_sprites_back_male
export(SpriteFrames) var battle_sprites_front_female
export(SpriteFrames) var battle_sprites_back_female



func get_class():
	return "PokemonDataGeneric"


