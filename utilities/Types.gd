extends Resource
class_name Types

enum TYPES {
	NORMAL,
	FIRE,
	WATER,
	ELECTRIC,
	GRASS,
	ICE,
	FIGHTING,
	POISON,
	GROUND,
	FLYING,
	PSYCHIC,
	BUG,
	ROCK,
	GHOST,
	DRAGON,
	DARK,
	STEEL,
	FAIRY
}

const icons = {
	TYPES.NORMAL : 		preload("res://Backend/Enums/Types/Icons/normal_icon.png"),
	TYPES.FIRE : 		preload("res://Backend/Enums/Types/Icons/fire_icon.png"),
	TYPES.WATER : 		preload("res://Backend/Enums/Types/Icons/water_icon.png"),
	TYPES.ELECTRIC : 	preload("res://Backend/Enums/Types/Icons/electric_icon.png"),
	TYPES.GRASS : 		preload("res://Backend/Enums/Types/Icons/grass_icon.png"),
	TYPES.ICE : 		preload("res://Backend/Enums/Types/Icons/ice_icon.png"),
	TYPES.FIGHTING : 	preload("res://Backend/Enums/Types/Icons/fighting_icon.png"),
	TYPES.POISON : 		preload("res://Backend/Enums/Types/Icons/poison_icon.png"),
	TYPES.GROUND : 		preload("res://Backend/Enums/Types/Icons/ground_icon.png"),
	TYPES.FLYING : 		preload("res://Backend/Enums/Types/Icons/flying_icon.png"),
	TYPES.PSYCHIC : 	preload("res://Backend/Enums/Types/Icons/psychic_icon.png"),
	TYPES.BUG : 		preload("res://Backend/Enums/Types/Icons/bug_icon.png"),
	TYPES.ROCK : 		preload("res://Backend/Enums/Types/Icons/rock_icon.png"),
	TYPES.GHOST : 		preload("res://Backend/Enums/Types/Icons/ghost_icon.png"),
	TYPES.DRAGON : 		preload("res://Backend/Enums/Types/Icons/dragon_icon.png"),
	TYPES.DARK : 		preload("res://Backend/Enums/Types/Icons/dark_icon.png"),
	TYPES.STEEL : 		preload("res://Backend/Enums/Types/Icons/steel_icon.png"),
	TYPES.FAIRY : 		preload("res://Backend/Enums/Types/Icons/fairy_icon.png")
}

export var EFFECTIVENESS = [
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 0 	, 1 	, 1 	, 0.5	, 1 	], # normal
	[1 	, 0.5	, 0.5	, 1 	, 2 	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 0.5	, 1 	, 0.5	, 1 	, 2 	, 1 	], # fire
	[1 	, 2 	, 0.5	, 1 	, 0.5	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 1 	, 1 	], # water
	[1 	, 1 	, 2 	, 0.5	, 0.5	, 1 	, 1 	, 1 	, 0 	, 2 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	], # electric
	[1 	, 0.5	, 2 	, 1 	, 0.5	, 1 	, 1 	, 0.5	, 2 	, 0.5	, 1 	, 0.5	, 2 	, 1 	, 0.5	, 1 	, 0.5	, 1 	], # grass
	[1 	, 0.5	, 0.5 	, 1 	, 2 	, 0.5	, 1 	, 1 	, 2 	, 2 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	], # ice
	[2 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 0.5	, 0.5	, 0.5	, 2 	, 0 	, 1 	, 2 	, 2 	, 0.5	], # fighting
	[1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 0.5	, 0.5	, 1 	, 1 	, 1 	, 0.5	, 0.5 	, 1 	, 1 	, 0 	, 2 	], # poison
	[1 	, 2 	, 1 	, 2 	, 0.5	, 1 	, 1 	, 2 	, 1 	, 0 	, 1 	, 0.5	, 2 	, 1 	, 1 	, 1 	, 2 	, 1 	], # ground
	[1 	, 1 	, 1 	, 0.5	, 2 	, 1 	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	, 0.5	, 1 	], # flying
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 2 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	, 1 	, 0 	, 0.5	, 1 	], # psychic
	[1 	, 0.5	, 1 	, 1 	, 2 	, 1 	, 0.5	, 0.5	, 1 	, 0.5	, 2 	, 2 	, 1 	, 0.5 	, 1 	, 2 	, 0.5	, 0.5	], # bug
	[1 	, 2 	, 1 	, 1 	, 1 	, 2 	, 0.5	, 1 	, 0.5	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	], # rock
	[0 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 1 	], # ghost
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 0 	], # dragon
	[1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 0.5	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 2 	, 1 	, 0.5	, 1 	, 0.5	], # dark
	[1 	, 0.5	, 0.5 	, 0.5	, 1 	, 2 	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 1 	, 1 	, 1 	, 0.5	, 2 	], # steel
	[1 	, 0.5	, 1 	, 1 	, 1 	, 1 	, 2 	, 0.5	, 1 	, 1 	, 1 	, 1 	, 1 	, 1 	, 2 	, 2 	, 0.5	, 1 	], # fairy
]
