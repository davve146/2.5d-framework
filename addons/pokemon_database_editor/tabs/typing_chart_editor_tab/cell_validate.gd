tool
extends LineEdit

onready var bg_color = $Color

func _on_Value_text_changed(new_text):
	pass

func _process(delta):
	match text:
		"0":
			bg_color.color = Color.black
		"0.":
			pass
		"0.5":
			bg_color.color = Color.crimson
		"2":
			bg_color.color = Color.forestgreen
		_:
			bg_color.color = Color8(255,255,255,0)
			text = ""
