extends Resource
class_name Typing

export(int) var id: int
export(String) var name: String

export var damage_relations = {
	"no_damage_to" : [],
	"half_damage_to" : [],
	"double_damage_to" : []
}

var names: Array
var pokemon: Array

export(Texture) var icon
